# kirby-plugin-carousel

Output a Bootstrap (V3) carousel in Kirby.

You can use it with image or Vimeo.

```
(carousel…)
(image: placeholder-1.png caption: image une)
(vimeo: http://vimeo.com/3432886)
(image: placeholder-2.png caption: image deux)
(image: placeholder-3.png caption: image trois test-label: value test)
(image: placeholder-4.png)
(image: placeholder-5.png)
(vimeo: https://vimeo.com/3432886 caption: This is a really nice video)
(…carousel)
```

### /!\ Requirement

* use `c::set('kirbytext.image.figure', false)`
* for vimeo compatibility: use https://github.com/jrue/Vimeo-jQuery-API
* for vimeo compatibility: add plugin’s script to your template `js(assets/plugins/carousel/js/carousel.js)`.

### Options

- `c::set('carousel.video.vimeo.ratio', '4by3 | 16by9')`
- `c::set('carousel.indicators', true)`
- `c::set('carousel.indicators.type', 'default | numeric')` [^1]
- `c::set('carousel.caption', true)`

[1]: Will require additionnal CSS.

### TODO:

* make it works even with `c::set('kirbytext.image.figure', true)`
* make it compatible with oEmbed plugin.
* eventually output prev/next glyphicons like the original Bootstrap carousel
